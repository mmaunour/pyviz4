# A concrete example

To launch pyviz in this example:
```sh
python3 genstate.py 
```

Several state files and a sequence file are created.

To launch ViZiR 4 in the sequence mode and generate the images:
```sh
vizir4 -seq -in file.meshb
```