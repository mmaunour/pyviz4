import pyviz

# declare a sequence list of state + output required 
s = pyviz.Sequence()

d = pyviz.State()
d.read("vizir.state")

# personalize you .state here
d.set('IsoOn',1)
d.disable('TextOn')
d.disable('LineOn')
d.set('SolOn',1)

# write it and save it to the sequence 
d.write('vizir-solon.state')

# add another state
d.set('IsoOn',1)
d.enable('Capping')
d.write('vizir-isoon.state')

# add in the sequence file
s.add('vizir.state', 'test1.png')
s.add('vizir-solon.state', 'test2.png')
s.add('vizir-isoon.state', 'test3.png')

s.write('vizir.seq')

print("State and sequence files have been successfully created")