To launch the two scripts
```sh
python3 change_boolean.py
```
and 

```sh
python3 change_palette.py
```

The directory [sample](https://gitlab.inria.fr/mmaunour/pyviz4/-/tree/master/examples/sample) contains a more concrete example with mesh and solution file to use with Vizir 4