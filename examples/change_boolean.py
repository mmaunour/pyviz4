import pyviz

# declare a sequence list of state + output required 
s = pyviz.Sequence()

d = pyviz.State()
d.read("vizir.state")

# personalize you .state here
d.disable('TextOn')
d.enable('LineOn')
d.set('IsoOn', 0)
d.set('SolOn',0)

# write it and save it to the sequence 
d.write('vizir-lineoff.state')
s.add('vizir-lineoff.state', 'out/test1.png')

s.write('vizir.seq')

