import numpy as np
import pyviz

ycut = np.linspace(0.1, 1., 10)

# Example of Vizir state file.
d = pyviz.State()
d.read("vizir.state")
d.set('IsoOn',0)
d.disable('TextOn')
d.enable('LineOn')

for y in ycut:
    d.set('CutPlaneEquation', 0., 1., 0., -y)
    d.write('output' + str(int(10*y)).rjust(2, '0') + '.state')

# Example of Vizir sequence file.
s = pyviz.Sequence()
for y in ycut:
    n = 'output' + str(int(10*y)).rjust(2, '0')
    s.add(n + '.state', n + '.png')
# Warning: the Vizir sequence file have to be named vizir.seq
s.write('vizir.seq')

#pyviz.run_vizir('vizir4', '/Users/jvanhare/Desktop/ite.15.800000.meshb', '/Users/jvanhare/Desktop/ite.15.800000.solb', with_seq=True)
