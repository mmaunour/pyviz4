import pyviz

# declare a sequence list of state + output required 
d = pyviz.State()
d.read("vizir.state")

# personalize you .state here
d.enable('UsePalette')
d.set('SolOn',0)

d.set('Palette',0,0.01,0.2,0.3,0.4)

  

# write it and save it to the sequence 
d.write('vizir-palette.state')

