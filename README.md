# pyviz 4

pyviz 4 offers functions to easily create scripts in python to generate state and sequence files useful for [Vizir 4](https://vizir.inria.fr/)

# How to install pyviz / deploy the wheel

Once the git clone is done, it is very easy to install it:

./create_whl.sh

# How to launch

Launch a script is very easy:

python3 name_of_the_file.py

# Examples

A directory [examples](https://gitlab.inria.fr/mmaunour/pyviz4/-/tree/master/examples) contains several examples.

Here is an example of script

```python
To import numpy as np
import pyviz

ycut = np.linspace(0.1, 1., 10)

# Example of Vizir state file.
d = pyviz.State()
d.read("vizir.state")
d.set('IsoOn',0)
d.disable('TextOn')
d.enable('LineOn')

for y in ycut:
    d.set('CutPlaneEquation', 0., 1., 0., -y)
    d.write('output' + str(int(10*y)).rjust(2, '0') + '.state')

# Example of Vizir sequence file.
s = pyviz.Sequence()
for y in ycut:
    n = 'output' + str(int(10*y)).rjust(2, '0')
    s.add(n + '.state', n + '.png')
# Warning: the Vizir sequence file have to be named vizir.seq
s.write('vizir.seq')
```

Use `enable` or `disable` to modify the booleans.

Use `set` to specify the other keywords for instance:

```python
d.set('Palette',0,0.1,0.2,0.3,0.4)
```

# About the keywords

All the keywords can be found in State.py. There are 3 types of keywords: boolean keywords, admissible keywords and admissible multiple lines keywords

## How to package and install the "library"

If people do not have this git repo, a wheel can be created to given to them

[More details on how to package: see this guide](https://packaging.python.org/tutorials/packaging-projects/)

python3 setup.py sdist bdist_wheel

It creates a build directory and a dist directory with the .whl file.

To install the "library"

python3 -m pip install pyviz-4-py3-none-any.whl 
