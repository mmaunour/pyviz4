import os
from distutils.spawn import find_executable
from subprocess import Popen, PIPE


def run_vizir(vizir4, msh, sol, **kwargs):
    vizir_cmd = find_executable(vizir4)
    if not vizir_cmd:
        raise SystemError('ViZiR executable not found. Try again!')

    if kwargs['with_seq']:
        p = Popen([vizir_cmd, '-seq', '-in', msh, '-sol', sol],
                  stdout=PIPE, stderr=PIPE)
    else:
        p = Popen([vizir_cmd, '-in', msh, '-sol', sol],
                  stdout=PIPE, stderr=PIPE)
    return p.communicate()

# EOF.
