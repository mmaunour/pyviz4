# Admissible keys.
Keys = {'WindowSize': 2, 'Center': 3, 'Rotation': 4, 'Translation': 3, 'CutPlaneEquation': 4, 'Fovy': 1,
        'SolOn' : 1, 'IsoOn' : 1, 'CurSol': 1, 'ithSol': 1, 'IsoSiz': 1, 
        'TessLevel': 1, 'ShaMod': 1, 'RenMod': 1, 'WireSiz': 1, 'Palette': 5, 'CutoffBelowVal': 1, 'CutoffAboveVal': 1}

# Admissible boolean keys.
BoolKeys = ['UseCutPlane', 'Capping', 'ShowCut',
            'PalOn', 'TextOn', 'LineOn', 'TessOn', 'LineSurfOn', 'TransOn', 'UsePalette',
            'EdgeOn','HideSurfOn', 'CutoffBelowOn', 'CutoffAboveOn']

# Admissible text keys.
TextKeys = {'Colormap': 1}

# Admissible multiple lines keys.
MulKeys = {'NumberOfHiddenRefObjects': 1}

def printBoolKeys():
  print("Here is the list of all admissible boolean keywords: ")
  print(*BoolKeys)

def printKeys():
  print("Here is the list of all admissible (non boolean) keywords: ")
  print(*Keys)

def cast(s):
  try:
    return int(s)
  except ValueError:
    return float(s)


class State:
  # Python class to represent Vizir state file.

  # Default constructor.
  def __init__(self):
    self.dict = {}

  # Read a Vizir state file.
  def read(self, fn):
    with open(fn, 'r') as fid:
      while True:
        l = fid.readline()
        if not l:
          break
        if l.startswith("#"): # ignore comments
          #print("ignore comment")
          continue
        if l == "\n":# empty line
          continue
        val = l.strip().split(' ')
        if(val[0]==''):
          continue

        for item in val:
          item.strip()
        if val[0].isalpha():
          key = val[0]
        else:
          for i, v in enumerate(val):
            val[i] = cast(v)
          self.dict[key] = val
        if key in MulKeys:
          val = fid.readline().strip()
          nbr = cast(val)
          val += '\n'
          for i in range(nbr):
            val += fid.readline().strip()
            val += '\n'
          self.dict[key] = val
        if key in TextKeys:
          self.dict[key] = fid.readline().strip()

    print("Reading file ", fn, " ok")

  # Write a Vizir state file.
  def write(self, fn):
    with open(fn, 'w') as fid:
      for key in self.dict.keys():
        fid.write(key + '\n')
        if key in MulKeys:
          fid.write(self.dict[key])
        elif key in TextKeys:
          fid.write(self.dict[key])
        else:
          for v in self.dict[key]:
            fid.write(str(v) + ' ')
        fid.write('\n')
    print("Writing file ", fn, " ok")
        

  # Set value from key.
  def set(self, key, *val):
        if key not in Keys and key not in TextKeys:
            print('  %% WARNING: You are trying to set the keyword "' +
                             key + '" which does not exist. Try again!')
            printKeys()
            return

        if key in TextKeys:
          self.dict[key] = "".join(val)
          return
        
        if Keys[key] != len(val):
            raise ValueError('You are trying to set the keyword "' + key + '" with ' + str(
                len(val)) + ' arguments intead of ' + str(Keys[key]) + '. Try again!')
        self.dict[key] = []
        for v in val:
            self.dict[key].append(v)

  def get(self, key):
    if key not in Keys:
      raise ValueError('You are trying to get the keyword "' +
                        key + '" which does not exist. Try again!')
      return self.dict[key] 

  # Enable a boolean key.
  def enable(self, key):
    if key not in BoolKeys:
      print('  %% WARNING: You are trying to disable the boolean keyword "' +
                        key + '" which does not exist. Try again! ')
      printBoolKeys()
      return
    self.dict[key] = [1]

  # Disable a boolean key.
  def disable(self, key):
    if key not in BoolKeys:
      print('  %% WARNING: You are trying to disable the boolean keyword "' +
                             key + '" which does not exist. Try again! ')
      printBoolKeys()
      return
    self.dict[key] = [0]

# EOF.
