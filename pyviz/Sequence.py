class Sequence:
    # Python class to represent Vizir sequence file.

    # Default constructor.
    def __init__(self):
        self.dict = {}

    def add(self, dat, png):
        self.dict[dat] = png

    # Write a Vizir sequence file.
    def write(self, fn):
        with open(fn, 'w') as fid:
            for key, val in self.dict.items():
                fid.write(key + ' ' + val + '\n')

# EOF.
