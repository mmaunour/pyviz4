rm -rf dist
rm -rf build
rm -rf /usr/local/lib/python*/site-packages/pyviz*

python3 setup.py sdist bdist_wheel

cd dist

python3 -m pip install pyviz-4-py3-none-any.whl
